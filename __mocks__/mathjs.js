const mathjs = jest.createMockFromModule("mathjs");

mathjs.compile = function(formula) {
  return {
    evaluate: () => 1,
  };
};

mathjs.derivative = function(formula, varName) {
  return {
    evaluate: () => 1,
  };
};

module.exports = mathjs;
