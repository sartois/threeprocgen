import { Vector3 } from "three";
import { map } from "../utils/mathHelper";

export default class Chaser {
  constructor({
    maxSpeed = 5,
    maxForce = 5,
    arriveThreshold = 100,
    neighborhoodRadius = 100,
    initialPosition = new Vector3(),
    initialVelocity = new Vector3(),
  }) {
    /**
     * @var {Number}
     */
    this.maxSpeed = maxSpeed;
    /**
     * @var {Number}
     */
    this.arriveThreshold = arriveThreshold;
    /**
     * @var {Number}
     */
    this.maxForce = maxForce;
    /**
     * @var {Number}
     */
    this.neighborhoodRadius = neighborhoodRadius;
    /**
     * @var {Vector3}
     */
    this.acceleration = new Vector3();
    /**
     * @var {Vector3}
     */
    this.position = initialPosition;
    /**
     * @var {Vector3}
     */
    this.velocity = initialVelocity;
  }

  update(delta = 1) {
    this.acceleration = this.acceleration.multiplyScalar(delta);
    this.velocity.add(this.acceleration);
    this.velocity.clampLength(0, this.maxSpeed);
    this.position.add(this.velocity);
    this.acceleration.multiplyScalar(0);
  }

  /**
   * @param {Vector3} force
   */
  applyForce(force) {
    this.acceleration.add(force);
  }

  /**
   * @param {Vector3} target
   * @returns {Vector3}
   */
  chase(target) {
    return this.reach(target, 0);
  }

  /**
   * @param {Vector3} target
   * @param {Number} threshold - Dist to target from which agent will start slowing down
   * @returns {Vector3}
   */
  reach(target, threshold = null) {
    const distThreshold = threshold !== null ? threshold : this.arriveThreshold;
    const desired = target.clone().sub(this.position);

    const d = desired.length();

    const trueMaxSpeed =
      d < distThreshold
        ? map(d, 0, distThreshold, 0, this.maxSpeed) //@todo apply 1D Nonlinear Transformations
        : this.maxSpeed;

    return desired
      .normalize()
      .multiplyScalar(trueMaxSpeed)
      .sub(this.velocity)
      .clampLength(0, this.maxForce);
  }

  /**
   * @todo Filter with octree here or pass a filtered list ?
   *
   * @param {Array} chasers
   * @param {(undefined|Number)} separation
   * @returns {Vector3}
   */
  separate(chasers, separation = this.neighborhoodRadius) {
    let { sum, closeChasers } = chasers.reduce(
      ({ sum, closeChasers }, chaser) => {
        const d = this.position.distanceTo(chaser.position);
        if (d !== 0 && d <= separation && this !== chaser) {
          const diff = this.position
            .clone()
            .sub(chaser.position)
            .normalize();
          sum.add(diff.divideScalar(d));
          closeChasers++;
        }
        return { sum, closeChasers };
      },
      { sum: new Vector3(), closeChasers: 0 }
    );

    if (closeChasers > 0) {
      return sum
        .divideScalar(closeChasers)
        .normalize()
        .multiplyScalar(this.maxSpeed)
        .sub(this.velocity)
        .clampLength(0, this.maxForce);
    }
    return new Vector3();
  }

  /**
   * @param {array} chasers
   * @returns {array}
   */
  getCloseChaser(chasers) {
    return chasers.filter(
      (chaser) =>
        this.position.distanceTo(chaser.position) <= this.neighborhoodRadius
    );
  }
}
