import { Vector3 } from "three";
import Follower from "./Follower";
import FlowFieldV3 from "../FlowField/FlowFieldV3";

export default class Boid extends Follower {
  constructor({
    separateFactor = 1.5,
    alignFactor = 1,
    cohesionFactor = 1,
    ...params
  }) {
    super(params);
    this.separateFactor = separateFactor;
    this.alignFactor = alignFactor;
    this.cohesionFactor = cohesionFactor;
  }
  /**
   * Flowfield might be used to bound the boid world
   *
   * @param {Array} boids
   * @param {FlowFieldV3} flowField
   */
  flock(boids = [], flowField = null) {
    boids = this.getCloseChaser(boids);

    const s = this.separate(boids);
    const a = this.align(boids);
    const c = this.cohesion(boids);

    s.multiplyScalar(this.separateFactor);
    a.multiplyScalar(this.alignFactor);
    c.multiplyScalar(this.cohesionFactor);

    [s, a, c].forEach((force) => this.applyForce(force));

    if (flowField instanceof FlowFieldV3) {
      this.follow(flowField);
    }
  }

  /**
   * @param {array} boids
   * @returns {Vector3}
   */
  align(boids) {
    let sum = new Vector3();
    if (boids.length === 0) return sum;
    boids.forEach((boid) => sum.add(boid.velocity));
    return sum
      .divideScalar(boids.length)
      .setLength(this.maxspeed)
      .sub(this.velocity)
      .clampLength(0, this.maxForce);
  }

  /**
   * @param {array} boids
   * @returns {Vector3}
   */
  cohesion(boids) {
    let sum = new Vector3();
    if (boids.length === 0) return sum;
    boids.forEach((boid) => sum.add(boid.position));
    sum.divideScalar(boids.length);
    return this.chase(sum);
  }
}
