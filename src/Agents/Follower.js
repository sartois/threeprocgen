import Chaser from "./Chaser";
import FlowFieldV3 from "../FlowField/FlowFieldV3";
import { Vector3 } from "three";

export default class Follower extends Chaser {
  /**
   * @param {(FlowFieldV3|Vector3)} flow
   */
  follow(flow) {
    let desired;
    if (flow instanceof FlowFieldV3) {
      desired = flow.lookup(this.position);
    } else if (flow.isVector3) {
      desired = flow;
    } else {
      desired = new Vector3();
    }
    const steer = desired.multiplyScalar(this.maxSpeed).sub(this.velocity);
    this.applyForce(steer.clampLength(0, this.maxForce));
  }

  // @todo path following
}
