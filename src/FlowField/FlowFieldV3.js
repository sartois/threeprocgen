import { Vector3 } from "three";
import FlowField from "./FlowField";

export default class FlowFieldV3 extends FlowField {
  fill(filler) {
    super.fill(filler, true);
  }

  /**
   * @param {Vector3} position
   * @returns {Vector3}
   */
  lookup(position) {
    if (this.cellSize <= 0) return new Vector3();
    if (position.x < 0 || position.x >= this.width) return new Vector3();
    if (position.y < 0 || position.y >= this.height) return new Vector3();

    const row = Math.floor(position.y / this.cellSize);
    const column = Math.floor(position.x / this.cellSize);
    const index = row * this.cols + column;

    if (!this.fields.hasOwnProperty(index)) return new Vector3();

    return this.fields[index].clone();
  }

  /**
   * @todo handle vectors in fields
   */
  toJSON() {
    return {
      useVector3: true,
      cellSize: this.cellSize,
      width: this.width,
      height: this.height,
      cols: this.cols,
      rows: this.rows,
      fields: this.fields.map(({ x, y, z }) => ({ x, y, z, isVector3: true })),
    };
  }
}
