export default class FlowField {
  /**
   * Compute col and row count.
   * May increase width and/or height by less than cell if width/height and cell are not multiple
   *
   * @param {Object} o
   * @param {int} o.width Grid width
   * @param {int} o.height Grid height
   * @param {int} o.cellSize
   * @param {Object} [o.filler=null] A function to fill the flowfield with
   * @param {Array} [o.fields=null] Do not use filler, set fields directly
   */
  constructor({ width, height, cellSize, fields = [], filler = null }) {
    let remainder;

    this.cellSize = cellSize;

    const w = Math.ceil(width),
      h = Math.ceil(height);

    this.width =
      (remainder = w % cellSize) === 0 ? w : w + (cellSize - remainder);
    this.height =
      (remainder = h % cellSize) === 0 ? h : h + (cellSize - remainder);
    this.cols = cellSize > 0 ? this.width / cellSize : 0;
    this.rows = cellSize > 0 ? this.height / cellSize : 0;
    this.fields = fields;

    filler && this.fields.length === 0 && this.fill(filler);
  }

  /**
   * Fill the flow field with callback
   *
   * @param {fill~filler} filler
   */
  /**
   * @callback fill~filler
   * @param {Object} o
   * @param {int} o.curRow
   * @param {int} o.curCol
   * @param {int} o.rows Rows number
   * @param {int} o.cols Cols number
   * @param {int} o.width realWidth
   * @param {int} o.height realHeight
   * @param {int} o.cellSize
   * @param {bool} o.useVector3
   * @returns {(number|Vector2)} The angle in radian or a vector
   */
  fill(filler, useVector3 = false) {
    if (typeof filler !== "function") {
      console.warn("Fill argument must be a function");
      return;
    }

    let { rows, cols, cellSize, width, height } = this;

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        this.fields[i * cols + j] = filler({
          curRow: i,
          curCol: j,
          rows,
          cols,
          width,
          height,
          cellSize,
          useVector3,
        });
      }
    }
  }

  /**
   * @param {*} o
   * @param {Number} o.x
   * @param {Number} o.y
   * @returns {null|Number}
   */
  lookup({ x, y }) {
    if (this.cellSize <= 0) return null;
    if (x < 0 || x >= this.width) return null;
    if (y < 0 || y >= this.height) return null;

    const row = Math.floor(y / this.cellSize);
    const column = Math.floor(x / this.cellSize);
    const index = row * this.cols + column;

    if (!this.fields.hasOwnProperty(index)) return null;

    return this.fields[index];
  }

  /**
   * @todo handle vectors in fields
   */
  toJSON() {
    return {
      cellSize: this.cellSize,
      width: this.width,
      height: this.height,
      cols: this.cols,
      rows: this.rows,
      fields: this.fields,
    };
  }
}
