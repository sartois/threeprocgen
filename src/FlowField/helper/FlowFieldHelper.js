import {
  LineSegments,
  BufferGeometry,
  Float32BufferAttribute,
  LineBasicMaterial,
  Color,
  Object3D,
  Vector3,
  ArrowHelper,
} from "three";

/**
 * Something like THREE.js helpers (See axesHelper, gridHelper, ...)
 * Build a grid mapped to the flowfield, and foreach cell draw an arrow from the angle
 */
export class FlowFieldHelper extends Object3D {
  /**
   * @param {FlowField} flowField
   * @param {(Color|string)} color
   * @param {boolean} showGrid
   */
  constructor(
    flowField,
    gridColor = new Color(0xff00ff),
    arrowColor = new Color(0xff00ff),
    showGrid = true
  ) {
    super();
    this.type = "FlowFieldHelper";
    if (showGrid) {
      this.add(new FlowFieldGrid(flowField, new Color(gridColor)));
    }
    this.add(this.addArrows(flowField, arrowColor));
  }

  /**
   * Warning, hardcoded (x,y) flowfield coord mappped to (x,z)
   *
   * @param {FlowField} flowField
   */
  addArrows(flowField, arrowColor) {
    const halfCell = flowField.cellSize / 2;
    const widthEnd = flowField.width;
    const heightEnd = flowField.height;
    const arrows = new Object3D();
    //memoize computed cos/sin
    const computed = {};

    for (let i = halfCell; i < heightEnd; i += flowField.cellSize) {
      for (let j = halfCell; j < widthEnd; j += flowField.cellSize) {
        const value = flowField.lookup({ x: j, y: i });
        let dir;
        if (value !== null) {
          if (value && value.isVector3) {
            if (value.length() > 0)
              arrows.add(
                new ArrowHelper(
                  new Vector3(value.x, 0, value.y),
                  new Vector3(j, 0, i),
                  halfCell,
                  arrowColor,
                  0.5 * halfCell,
                  0.4 * halfCell
                )
              );
          } else {
            if (!computed[value]) {
              computed[value] = new Vector3(
                Math.cos(value),
                0,
                Math.sin(value)
              );
            }
            dir = computed[value].clone();

            arrows.add(
              new ArrowHelper(
                dir,
                new Vector3(j, 0, i),
                halfCell,
                arrowColor,
                0.5 * halfCell,
                0.4 * halfCell
              )
            );
          }
        }
      }
    }

    return arrows;
  }
}

export class FlowFieldGrid extends LineSegments {
  /**
   * @param {FlowField} flowField
   * @param {Color} color
   */
  constructor(flowField, color) {
    const vertices = [];
    const colors = [];
    let k = 0;

    for (let i = 0; i <= flowField.height; i += flowField.cellSize) {
      vertices.push(0, 0, i, flowField.width, 0, i);
      color.toArray(colors, k);
      k += 3;
      color.toArray(colors, k);
      k += 3;
    }

    for (let j = 0; j <= flowField.width; j += flowField.cellSize) {
      vertices.push(j, 0, 0, j, 0, flowField.height);
      color.toArray(colors, k);
      k += 3;
      color.toArray(colors, k);
      k += 3;
    }

    const geometry = new BufferGeometry();
    geometry.setAttribute("position", new Float32BufferAttribute(vertices, 3));
    geometry.setAttribute("color", new Float32BufferAttribute(colors, 3));

    const material = new LineBasicMaterial({
      vertexColors: true,
      toneMapped: false,
    });

    super(geometry, material);

    this.type = "FlowFieldGrid";
  }
}
