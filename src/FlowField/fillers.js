import { compile, derivative } from "mathjs";
import { Vector3 } from "three";

/**
 * @param {Number} angle
 * @param {bool} useVector3
 */
function toReturn(angle, useVector3) {
  if (angle === null) {
    return useVector3 ? new Vector3() : null;
  }
  return useVector3 ? new Vector3(Math.cos(angle), Math.sin(angle)) : angle;
}

/**
 * @param {*} O
 * @param {int} O.curRow
 * @param {int} O.rows
 * @returns {number}
 */
export function halfCircleFiller({ curRow, rows, useVector3 }) {
  if (rows === 0) return useVector3 ? new Vector3() : 0;
  const angle = -(curRow / rows) * Math.PI;
  return toReturn(angle, useVector3);
}

/**
 * Factory function, build the flowfield filler
 *
 * The returned function will fill the flowfield with some curve local slope
 * Compute the formula derivative. Must use "x" as variable name in the passed function.
 *
 * Warning, mathjs is not packaged with that bundle, include it if needed
 *
 * [See mathjs docs]{@link https://mathjs.org/docs/expressions/algebra.html}
 *
 * @param {Object} O
 * @param {string} O.formula - Some math expression with "x" as var. Must be differentiable
 * @param {int} O.width - Flowfield width
 * @param {int} O.height - Flowfield height
 * @param {int} O.cell - Flowfield cell size
 * @param {bool} O.fulfill - Fill cells that are not on formula's curve
 * @param {number} O.maxTilt - Express max tilt applied to the most distant row
 * @returns {Function}
 */
export function curveDerivativeFiller({
  formula,
  width,
  height,
  cell,
  fulfill = true,
  maxTilt = 0.5 * Math.PI,
}) {
  if (!width || width <= 0 || !cell || cell <= 0) return null;

  const curve = compile(formula);
  const curveDerivative = derivative(formula, "x");
  const plot = {};
  const tiltFactor = maxTilt / width / cell;

  //plot point for that curve
  //store nearest row for that computed y
  for (let i = 0; i < width; i += cell) {
    const result = curve.evaluate({ x: i });
    plot[i] =
      result >= 0 && result <= height && cell !== 0
        ? Math.floor(result / cell)
        : null;
  }

  return function fill({ curRow, curCol, rows, cellSize, useVector3 }) {
    if (rows === 0) return 0;

    const x = curCol * cellSize;
    const slope = curveDerivative.evaluate({ x });
    let angle = Math.atan(slope) || 0;

    if (plot[x] === curRow) {
      return toReturn(angle, useVector3);
    } else if (!fulfill) {
      return toReturn(null, useVector3);
    }
    // get distance from current row
    const dist = curRow - plot[x];
    //tilt angle
    angle = tiltFactor * dist + angle;
    return toReturn(angle, useVector3);
  };
}

export function edgeRepulsionFiller({ curRow, curCol, rows, cols, cellSize }) {
  if (curRow === 0 && curCol === 0) return; //go bottom right
  if (curRow === 0 && curCol === cols - 1) return; //go bottom left
  if (curRow === rows - 1 && curCol === 0) return; //go up right
  if (curRow === rows - 1 && curCol === cols - 1) return; //go up left
  if (curRow === 0) return; ///go down
  if (curRow === rows - 1) return; //go up
  if (curCol === 0) return; //go right
  if (curCol === cols - 1) return; //go left
}
