class Stack {
  constructor() {
    this.elements = [];
  }
  /**
   * @param {*} state
   */
  push(state) {
    return this.elements.push(state);
  }
  /**
   * @returns {*}
   */
  pop() {
    if (this.elements.length === 0) {
      return null;
    }
    return this.elements.pop();
  }
}

export default Stack;
