function getRowMajorOffset(x, y, Cols) {
  return x * Cols + y;
}

function getRowMajorOffset3D(x, y, z, Cols, Depth) {
  return z + Depth * (y + Cols * x);
}

/**
 * @todo test memoization perf ?
 */
function getOffset(...args) {
  if (args.length === 3) return getRowMajorOffset(...args);
  if (args.length === 5) return getRowMajorOffset3D(...args);
  return null;
}

export { getOffset };
