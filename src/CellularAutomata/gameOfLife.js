import { getOffset } from "../utils/rowMajor";

/**
 * Calculate the number of live neighbors
 *
 * @param {array} data
 * @param {int} x
 * @param {int} y
 * @param {int} columnsNb
 */
function countNeighbors(data, x, y, columnsNb) {
  let neighbors = 0;
  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      let flatMatrixIndex = getOffset(x + i, y + j, columnsNb);
      if (data.length < flatMatrixIndex) {
        throw new Error(
          `Indexoutofboundsexception, x: ${x + i}, y:${y + i}, data length: ${
            data.length
          }, flat index: ${flatMatrixIndex}`
        );
      }
      neighbors += data[flatMatrixIndex].state;
    }
  }

  // Correct by subtracting the cell state itself.
  neighbors -= data[getOffset(x, y, columnsNb)].state;

  return neighbors;
}

function theRuleOfLife(cellVal, neighborsNb) {
  const Ei = 2;
  const Eu = 3;
  const Fi = 3;
  const Fu = 3;

  if (cellVal) {
    return neighborsNb >= Ei && neighborsNb <= Eu ? 1 : 0;
  } else {
    return neighborsNb >= Fi && neighborsNb <= Fu ? 1 : 0;
  }
}

function getNextCell(currentState, nextState, age) {
  if (currentState !== nextState) {
    return {
      state: nextState,
      age: 0,
    };
  }

  return {
    state: currentState,
    age: ++age,
  };
}

export { countNeighbors, theRuleOfLife, getNextCell };
