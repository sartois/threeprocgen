import * as THREE from "three";
import Stack from "../utils/Stack";
import TurtleState from "./TurtleState";

export default class LSystem {
  constructor() {
    this.THE_STRING = "";
    this.stack = new Stack();
    this.currentState = null;
    this.startPlace = null;
    this.maxIteration = 0;
    this.currentIteration = 0;
    this.delta = 0;
    this.widthDecrease = 0;
    this.contractionRatio = 0;
    this.initialWidth = 0;
    this.initialRadius = 0;

    this.productions = {};
    this.productionKeys = [];

    this.zAxis = new THREE.Vector3(0, 0, 1);
    this.xAxis = new THREE.Vector3(1, 0, 0);
    this.yAxis = new THREE.Vector3(0, 1, 0);

    this.branchesGeometry = new THREE.Geometry();
    this.leafsGeometry = new THREE.Geometry();
  }

  /**
   * @param {THREE.Vector3} place
   * @returns {LSystem}
   */
  at(place) {
    this.startPlace = place;
    return this;
  }

  create({
    n,
    delta,
    widthDecrease,
    contractionRatio,
    axiom,
    productions,
    initialWidth,
    initialRadius,
  }) {
    this.maxIteration = n;
    this.delta = delta;
    this.widthDecrease = widthDecrease;
    this.contractionRatio = contractionRatio;
    this.productions = productions;
    this.productionKeys = Object.keys(this.productions);
    this.initialWidth = initialWidth;
    this.initialRadius = initialRadius;

    this.buildTheString(axiom);

    return this.render();
  }

  buildTheString(currentString) {
    this.currentIteration++;
    const newString = this.replaceWithProductions(currentString);
    if (this.currentIteration === this.maxIteration) {
      this.THE_STRING = newString;
    } else {
      this.buildTheString(newString);
    }
  }

  replaceWithProductions(currentString) {
    const test = currentString.split("").map((currentChar) => {
      return this.productionKeys.includes(currentChar)
        ? this.productions[currentChar]
        : currentChar;
    });
    return test.join("");
  }

  /**
   * @returns {Object}
   */
  render() {
    this.initState();
    this.THE_STRING.split("").forEach((currentChar) => {
      switch (currentChar) {
        case "[":
          this.pushState();
          break;
        case "]":
          this.popState();
          break;
        case "&":
          this.rotateTurtle("z");
          break;
        case "^":
          this.rotateTurtle("z", "-");
          break;
        case "/":
          this.rotateTurtle("x");
          break;
        case "\\":
          this.rotateTurtle("x", "-");
          break;
        case "+":
          this.rotateTurtle("y");
          break;
        case "-":
          this.rotateTurtle("y", "-");
          break;
        case "!":
          this.decrementSize();
          break;
        case "F":
          this.internode();
          break;
        case "L":
          this.addLeaf();
          break;
        case "A":
          //do nothing
          break;
        case "S":
          //do nothing
          break;

        default:
          console.log(`Unknown letter ${currentChar}`);
          break;
      }
    });

    return {
      leafs: this.leafsGeometry,
      branches: this.branchesGeometry,
    };
  }

  initState() {
    this.currentState = new TurtleState({
      segmentStart: this.startPlace.clone(),
      segmentEnd: new THREE.Vector3(0, 0, 0),
      lookAt: new THREE.Vector3(0, 1, 0),
      width: this.initialWidth,
      radius: this.initialRadius,
    });
  }

  pushState() {
    this.stack.push(this.currentState.clone());
  }

  popState() {
    this.currentState = this.stack.pop();
  }

  rotateTurtle(axis, dir = "+") {
    if (axis === "z") {
      this.currentState.lookAt.applyAxisAngle(
        this.zAxis,
        dir === "+" ? this.delta : -this.delta
      );
    } else if (axis === "y") {
      this.currentState.lookAt.applyAxisAngle(
        this.yAxis,
        dir === "+" ? this.delta : -this.delta
      );
    } else {
      this.currentState.lookAt.applyAxisAngle(
        this.xAxis,
        dir === "+" ? this.delta : -this.delta
      );
    }
  }

  decrementSize() {
    this.currentState.width *= this.widthDecrease;
    this.currentState.radius *= this.contractionRatio;
  }

  addLeaf() {
    const leafGeom = new THREE.PlaneGeometry(0.2, 0.1);
    //Align leaf on the ground
    leafGeom.translate(0, 0.05, 0);

    const leafMesh = new THREE.Mesh(leafGeom);
    //parallel to the ground
    leafMesh.rotateX(Math.PI / 2);

    //put leaf at segment end
    leafMesh.position.copy(this.currentState.segmentEnd);

    //orient with lookat
    // leafMesh.rotateY(angle)

    this.mergeWithOtherLeafs(leafMesh);
  }

  getSegmentCenter(segmentStart, segmentEnd) {
    return new THREE.Vector3(
      (segmentStart.x + segmentEnd.x) / 2,
      (segmentStart.y + segmentEnd.y) / 2,
      (segmentStart.z + segmentEnd.z) / 2
    );
  }

  internode() {
    //move position
    //compute segment end
    const lookAtClone = this.currentState.lookAt.clone();
    lookAtClone.normalize().multiplyScalar(this.currentState.width);

    const start = this.currentState.segmentStart.clone();
    this.currentState.segmentEnd = start.add(lookAtClone);

    //draw branch from start to end
    this.addBranch({ ...this.currentState });

    //move the turtle
    this.currentState.segmentStart = this.currentState.segmentEnd.clone();
  }

  addBranch({ radius, width, segmentStart, lookAt }) {
    const branchGeometry = new THREE.CylinderGeometry(radius, radius, width);
    //Align on the ground
    branchGeometry.translate(0, width / 2, 0);

    const branchMesh = new THREE.Mesh(branchGeometry);
    //then go to actual position
    branchMesh.position.copy(segmentStart);
    //orient the branch
    branchMesh.quaternion.setFromUnitVectors(this.yAxis, lookAt.normalize());

    this.mergeWithTrunk(branchMesh);
  }

  /**
   * @param {THREE.Mesh} leafMesh
   */
  mergeWithOtherLeafs(leafMesh) {
    leafMesh.updateMatrix();
    this.leafsGeometry.merge(leafMesh.geometry, leafMesh.matrix);
  }

  /**
   * @param {THREE.Mesh} branchMesh
   */
  mergeWithTrunk(branchMesh) {
    branchMesh.updateMatrix();
    this.branchesGeometry.merge(branchMesh.geometry, branchMesh.matrix);
  }
}
