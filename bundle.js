'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var THREE = require('three');

/**
 * @see https://processing.org/reference/constrain_.html
 *
 * @param {*} n
 * @param {*} low
 * @param {*} high
 * @returns {int}
 */
function constrain(n, low, high) {
  return Math.round(Math.max(Math.min(n, high), low));
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 * @returns {Number}
 */
function getRandomRange(min, max) {
  return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 *
 * Using Math.round() will give you a non-uniform distribution!
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 * @returns {int}
 */
function getRandomRangeInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * @see https://github.com/processing/p5.js/blob/1.1.9/src/math/calculation.js#L408
 *
 * @param  {Number} value  the incoming value to be converted
 * @param  {Number} start1 lower bound of the value's current range
 * @param  {Number} stop1  upper bound of the value's current range
 * @param  {Number} start2 lower bound of the value's target range
 * @param  {Number} stop2  upper bound of the value's target range
 * @param  {Boolean} [withinBounds] constrain the value to the newly mapped range
 * @return {Number}
 */
function map(value, start1, stop1, start2, stop2, withinBounds) {
  const newval =
    ((value - start1) / (stop1 - start1)) * (stop2 - start2) + start2;
  if (!withinBounds) {
    return newval;
  }
  return start2 < stop2
    ? constrain(newval, start2, stop2)
    : constrain(newval, stop2, start2);
}

const mathHelper = {
  constrain,
  getRandomRange,
  getRandomRangeInt,
  map,
};

class FlowField {
  constructor({
    width,
    height,
    resolution = 50,
    edgeRepulsion = true,
    zoneRepulsion = null,
  }) {
    this.resolution = resolution;
    this.width = width;
    this.height = height;
    this.widthMax = Math.round(this.width / 2);
    this.widthMin = -this.widthMax;
    this.heightMax = Math.round(this.height / 2);
    this.heightMin = -this.heightMax;
    this.cols = Math.ceil(width / resolution);
    this.rows = Math.ceil(height / resolution);
    this.edgeRepulsion = edgeRepulsion;
    this.zoneRepulsion = ["nw", "ne", "se", "sw"].some((zone) =>
      zone.includes(zoneRepulsion)
    )
      ? zoneRepulsion
      : null;
    this.fields = [];

    this._init();
  }

  _init() {
    for (let i = 0; i < this.cols; i++) {
      for (let j = 0; j < this.rows; j++) {
        if (!Array.isArray(this.fields[i])) {
          this.fields[i] = [];
        }
        this.fields[i][j] = new THREE.Vector2();
      }
    }

    if (this.edgeRepulsion) {
      const lastCols = this.cols - 1;
      const lastRow = this.rows - 1;
      for (let i = 0; i < this.cols; i++) {
        for (let j = 0; j < this.rows; j++) {
          if (i === 0 && j === 0) {
            this.fields[0][0].set(1, -1);
          } else if (i === lastCols && j === 0) {
            this.fields[lastCols][0].set(-1, -1);
          } else if (i === 0 && j === lastRow) {
            this.fields[0][lastRow].set(1, 1);
          } else if (i === lastCols && j === lastRow) {
            this.fields[lastCols][lastRow].set(-1, 1);
          } else if (i === 0) {
            this.fields[0][j].set(1, 0);
          } else if (i === lastCols) {
            this.fields[lastCols][j].set(-1, 0);
          } else if (j === 0) {
            this.fields[i][0].set(0, 1);
          } else if (j === lastRow) {
            this.fields[i][lastRow].set(0, -1);
          }
        }
      }
    }

    if (this.zoneRepulsion) {
      if (this.zoneRepulsion === "sw") {
        const colStart = 1;
        const colEnd = Math.ceil(this.cols / 3);
        const rowStart = Math.round(this.rows / 2);
        const rowEnd = this.rows - 1;

        for (let ii = colStart; ii < colEnd; ii++) {
          for (let jj = rowStart; jj < rowEnd; jj++) {
            this.fields[ii][jj].set(1, 1);
          }
        }
      }
    }
  }

  lookup(target) {
    if (!target || !target.x || !target.y) {
      console && console.debug("Flowfield.lookup, bad target : ", target);
      return new THREE.Vector2();
    }

    const x = mathHelper.map(
      target.x,
      this.widthMin,
      this.widthMax,
      0,
      this.width,
      true
    );

    const y = mathHelper.map(
      target.y,
      this.heightMin,
      this.heightMax,
      0,
      this.height,
      true
    );

    const column = Math.floor(x / this.resolution);
    const row = Math.floor(y / this.resolution);

    try {
      return this.fields[column][row].clone();
    } catch (e) {
      console.debug("Error while fetching flowField", this.fields, column, row);
      return new THREE.Vector2();
    }
  }
}

class Boid {
  constructor({
    maxSpeed = 4,
    maxSteerForce = 0.1,
    initialVelocity = new THREE.Vector2(0.5 - Math.random(), 0.5 - Math.random()),
    initialPosition = new THREE.Vector2(0.5 - Math.random(), 0.5 - Math.random()),
    worldBounds = new THREE.Vector2(),
    neighborhoodRadius = 100,
    randomness = 0.6,
  }) {
    this.maxSpeed = maxSpeed;
    this.target = null;
    this.neighborhoodRadius = neighborhoodRadius;
    this.maxSteerForce = maxSteerForce;
    this.randomness = randomness;

    this.acceleration = new THREE.Vector2();
    this.dummy = new THREE.Vector2();
    this.steer = new THREE.Vector2();
    this.position = initialPosition;
    this.velocity = initialVelocity;
    this.worldBounds = worldBounds;

    this.x = 0;
    this.y = 0;
  }

  /**
   * @param {Vector2} target
   */
  setGoal(target) {
    this.target = target;
  }

  /**
   * @param {Array} boids
   * @param {FlowField} flowFields
   * @returns {Vector2}
   */
  run(boids = [], flowFields = null) {
    if (Math.random() > this.randomness) {
      this._flock(boids);
    }

    this._move(flowFields);

    this.x = this.position.x;
    this.y = this.position.y;

    return this.position;
  }

  _flock(boids) {
    if (this.target) {
      this.acceleration.add(this._reach(this.target, 0.005));
    }

    this.acceleration.add(this._alignment(boids));
    this.acceleration.add(this._cohesion(boids));
    this.acceleration.add(this._separation(boids));
  }

  /**
   * @param {FlowField} flowFields
   */
  _move(flowFields) {
    if (flowFields) {
      this.acceleration.add(flowFields.lookup(this.position));
    }

    this.velocity.add(this.acceleration);
    var l = this.velocity.length();
    if (l > this.maxSpeed) {
      this.velocity.divideScalar(l / this.maxSpeed);
    }

    this.position.add(this.velocity);
    this.acceleration.set(0, 0);

    return this._validatePosition();
  }

  _validatePosition() {
    let { x, y } = this.position;
    x = isNaN(x) ? 0 : x;
    y = isNaN(y) ? 0 : y;
    this.position.set(x, y);
    return this.position.clone();
  }

  _alignment(boids) {
    let boid,
      count = 0,
      distance,
      i;
    const velSum = new THREE.Vector2();

    for (i = 0; i < boids.length; i++) {
      if (Math.random() > this.randomness) continue;
      boid = boids[i];
      distance = boid.position.distanceTo(this.position);
      if (distance > 0 && distance <= this.neighborhoodRadius) {
        velSum.add(boid.velocity);
        count++;
      }
    }

    if (count > 0) {
      velSum.divideScalar(count);
      var l = velSum.length();
      if (l > this.maxSteerForce) {
        velSum.divideScalar(l / this.maxSteerForce);
      }
    }

    return velSum;
  }

  _cohesion(boids) {
    let count = 0,
      boid,
      distance,
      i;
    const posSum = new THREE.Vector2();

    for (i = 0; i < boids.length; i++) {
      if (Math.random() > this.randomness) continue;

      boid = boids[i];
      distance = boid.position.distanceTo(this.position);

      if (distance > 0 && distance <= this.neighborhoodRadius) {
        posSum.add(boid.position);
        count++;
      }
    }

    if (count > 0) {
      posSum.divideScalar(count);
    }

    this.steer.set(0, 0);
    this.steer.subVectors(posSum, this.position);
    var l = this.steer.length();

    if (l > this.maxSteerForce) {
      this.steer.divideScalar(l / this.maxSteerForce);
    }

    return this.steer;
  }

  _separation(boids) {
    let boid, distance, i;
    const posSum = new THREE.Vector2();
    const repulse = new THREE.Vector2();

    for (i = 0; i < boids.length; i++) {
      if (Math.random() > this.randomness) continue;

      boid = boids[i];
      distance = boid.position.distanceTo(this.position);

      if (distance > 0 && distance <= this.neighborhoodRadius) {
        repulse.subVectors(this.position, boid.position);
        repulse.normalize();
        repulse.divideScalar(distance);
        posSum.add(repulse);
      }
    }

    return posSum;
  }

  _reach(target, amount) {
    this.steer.set(0, 0);
    this.steer.subVectors(target, this.position);
    this.steer.multiplyScalar(amount);
    return this.steer;
  }
}

class Stack {
  constructor() {
    this.elements = [];
  }
  /**
   * @param {*} state
   */
  push(state) {
    return this.elements.push(state);
  }
  /**
   * @returns {*}
   */
  pop() {
    if (this.elements.length === 0) {
      return null;
    }
    return this.elements.pop();
  }
}

class TurtleState {
  constructor({ segmentStart, segmentEnd, lookAt, width, radius }) {
    this.segmentStart = segmentStart;
    this.segmentEnd = segmentEnd;
    this.lookAt = lookAt;
    this.width = width;
    this.radius = radius;
  }

  clone() {
    return new TurtleState({
      segmentStart: this.segmentStart.clone(),
      segmentEnd: this.segmentEnd.clone(),
      lookAt: this.lookAt.clone(),
      width: this.width,
      radius: this.radius,
    })
  }
}

class LSystem {
  constructor() {
    this.THE_STRING = "";
    this.stack = new Stack();
    this.currentState = null;
    this.startPlace = null;
    this.maxIteration = 0;
    this.currentIteration = 0;
    this.delta = 0;
    this.widthDecrease = 0;
    this.contractionRatio = 0;
    this.initialWidth = 0;
    this.initialRadius = 0;

    this.productions = {};
    this.productionKeys = [];

    this.zAxis = new THREE.Vector3(0, 0, 1);
    this.xAxis = new THREE.Vector3(1, 0, 0);
    this.yAxis = new THREE.Vector3(0, 1, 0);

    this.branchesGeometry = new THREE.Geometry();
    this.leafsGeometry = new THREE.Geometry();
  }

  /**
   * @param {THREE.Vector3} place
   * @returns {LSystem}
   */
  at(place) {
    this.startPlace = place;
    return this;
  }

  create({
    n,
    delta,
    widthDecrease,
    contractionRatio,
    axiom,
    productions,
    initialWidth,
    initialRadius,
  }) {
    this.maxIteration = n;
    this.delta = delta;
    this.widthDecrease = widthDecrease;
    this.contractionRatio = contractionRatio;
    this.productions = productions;
    this.productionKeys = Object.keys(this.productions);
    this.initialWidth = initialWidth;
    this.initialRadius = initialRadius;

    this.buildTheString(axiom);

    return this.render();
  }

  buildTheString(currentString) {
    this.currentIteration++;
    const newString = this.replaceWithProductions(currentString);
    if (this.currentIteration === this.maxIteration) {
      this.THE_STRING = newString;
    } else {
      this.buildTheString(newString);
    }
  }

  replaceWithProductions(currentString) {
    const test = currentString.split("").map((currentChar) => {
      return this.productionKeys.includes(currentChar)
        ? this.productions[currentChar]
        : currentChar;
    });
    return test.join("");
  }

  /**
   * @returns {Object}
   */
  render() {
    this.initState();
    this.THE_STRING.split("").forEach((currentChar) => {
      switch (currentChar) {
        case "[":
          this.pushState();
          break;
        case "]":
          this.popState();
          break;
        case "&":
          this.rotateTurtle("z");
          break;
        case "^":
          this.rotateTurtle("z", "-");
          break;
        case "/":
          this.rotateTurtle("x");
          break;
        case "\\":
          this.rotateTurtle("x", "-");
          break;
        case "+":
          this.rotateTurtle("y");
          break;
        case "-":
          this.rotateTurtle("y", "-");
          break;
        case "!":
          this.decrementSize();
          break;
        case "F":
          this.internode();
          break;
        case "L":
          this.addLeaf();
          break;
        case "A":
          //do nothing
          break;
        case "S":
          //do nothing
          break;

        default:
          console.log(`Unknown letter ${currentChar}`);
          break;
      }
    });

    return {
      leafs: this.leafsGeometry,
      branches: this.branchesGeometry,
    };
  }

  initState() {
    this.currentState = new TurtleState({
      segmentStart: this.startPlace.clone(),
      segmentEnd: new THREE.Vector3(0, 0, 0),
      lookAt: new THREE.Vector3(0, 1, 0),
      width: this.initialWidth,
      radius: this.initialRadius,
    });
  }

  pushState() {
    this.stack.push(this.currentState.clone());
  }

  popState() {
    this.currentState = this.stack.pop();
  }

  rotateTurtle(axis, dir = "+") {
    if (axis === "z") {
      this.currentState.lookAt.applyAxisAngle(
        this.zAxis,
        dir === "+" ? this.delta : -this.delta
      );
    } else if (axis === "y") {
      this.currentState.lookAt.applyAxisAngle(
        this.yAxis,
        dir === "+" ? this.delta : -this.delta
      );
    } else {
      this.currentState.lookAt.applyAxisAngle(
        this.xAxis,
        dir === "+" ? this.delta : -this.delta
      );
    }
  }

  decrementSize() {
    this.currentState.width *= this.widthDecrease;
    this.currentState.radius *= this.contractionRatio;
  }

  addLeaf() {
    const leafGeom = new THREE.PlaneGeometry(0.2, 0.1);
    //Align leaf on the ground
    leafGeom.translate(0, 0.05, 0);

    const leafMesh = new THREE.Mesh(leafGeom);
    //parallel to the ground
    leafMesh.rotateX(Math.PI / 2);

    //put leaf at segment end
    leafMesh.position.copy(this.currentState.segmentEnd);

    //orient with lookat
    // leafMesh.rotateY(angle)

    this.mergeWithOtherLeafs(leafMesh);
  }

  getSegmentCenter(segmentStart, segmentEnd) {
    return new THREE.Vector3(
      (segmentStart.x + segmentEnd.x) / 2,
      (segmentStart.y + segmentEnd.y) / 2,
      (segmentStart.z + segmentEnd.z) / 2
    );
  }

  internode() {
    //move position
    //compute segment end
    const lookAtClone = this.currentState.lookAt.clone();
    lookAtClone.normalize().multiplyScalar(this.currentState.width);

    const start = this.currentState.segmentStart.clone();
    this.currentState.segmentEnd = start.add(lookAtClone);

    //draw branch from start to end
    this.addBranch({ ...this.currentState });

    //move the turtle
    this.currentState.segmentStart = this.currentState.segmentEnd.clone();
  }

  addBranch({ radius, width, segmentStart, lookAt }) {
    const branchGeometry = new THREE.CylinderGeometry(radius, radius, width);
    //Align on the ground
    branchGeometry.translate(0, width / 2, 0);

    const branchMesh = new THREE.Mesh(branchGeometry);
    //then go to actual position
    branchMesh.position.copy(segmentStart);
    //orient the branch
    branchMesh.quaternion.setFromUnitVectors(this.yAxis, lookAt.normalize());

    this.mergeWithTrunk(branchMesh);
  }

  /**
   * @param {THREE.Mesh} leafMesh
   */
  mergeWithOtherLeafs(leafMesh) {
    leafMesh.updateMatrix();
    this.leafsGeometry.merge(leafMesh.geometry, leafMesh.matrix);
  }

  /**
   * @param {THREE.Mesh} branchMesh
   */
  mergeWithTrunk(branchMesh) {
    branchMesh.updateMatrix();
    this.branchesGeometry.merge(branchMesh.geometry, branchMesh.matrix);
  }
}

exports.Boid = Boid;
exports.FlowField = FlowField;
exports.LSystem = LSystem;
exports.mathHelper = mathHelper;
