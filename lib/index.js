'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var THREE = require('three');
var mathjs = require('mathjs');

/**
 * @see https://processing.org/reference/constrain_.html
 *
 * @param {*} n
 * @param {*} low
 * @param {*} high
 * @returns {int}
 */
function constrain(n, low, high) {
  return Math.round(Math.max(Math.min(n, high), low));
}

/**
 * @see https://github.com/processing/p5.js/blob/1.1.9/src/math/calculation.js#L408
 *
 * @param  {Number} value  the incoming value to be converted
 * @param  {Number} start1 lower bound of the value's current range
 * @param  {Number} stop1  upper bound of the value's current range
 * @param  {Number} start2 lower bound of the value's target range
 * @param  {Number} stop2  upper bound of the value's target range
 * @param  {Boolean} [withinBounds] constrain the value to the newly mapped range
 * @return {Number}
 */
function map(value, start1, stop1, start2, stop2, withinBounds) {
  const newval =
    ((value - start1) / (stop1 - start1)) * (stop2 - start2) + start2;
  if (!withinBounds) {
    return newval;
  }
  return start2 < stop2
    ? constrain(newval, start2, stop2)
    : constrain(newval, stop2, start2);
}

class Chaser {
  constructor({
    maxSpeed = 5,
    maxForce = 5,
    arriveThreshold = 100,
    neighborhoodRadius = 100,
    initialPosition = new THREE.Vector3(),
    initialVelocity = new THREE.Vector3(),
  }) {
    /**
     * @var {Number}
     */
    this.maxSpeed = maxSpeed;
    /**
     * @var {Number}
     */
    this.arriveThreshold = arriveThreshold;
    /**
     * @var {Number}
     */
    this.maxForce = maxForce;
    /**
     * @var {Number}
     */
    this.neighborhoodRadius = neighborhoodRadius;
    /**
     * @var {Vector3}
     */
    this.acceleration = new THREE.Vector3();
    /**
     * @var {Vector3}
     */
    this.position = initialPosition;
    /**
     * @var {Vector3}
     */
    this.velocity = initialVelocity;
  }

  update(delta = 1) {
    this.acceleration = this.acceleration.multiplyScalar(delta);
    this.velocity.add(this.acceleration);
    this.velocity.clampLength(0, this.maxSpeed);
    this.position.add(this.velocity);
    this.acceleration.multiplyScalar(0);
  }

  /**
   * @param {Vector3} force
   */
  applyForce(force) {
    this.acceleration.add(force);
  }

  /**
   * @param {Vector3} target
   * @returns {Vector3}
   */
  chase(target) {
    return this.reach(target, 0);
  }

  /**
   * @param {Vector3} target
   * @param {Number} threshold - Dist to target from which agent will start slowing down
   * @returns {Vector3}
   */
  reach(target, threshold = null) {
    const distThreshold = threshold !== null ? threshold : this.arriveThreshold;
    const desired = target.clone().sub(this.position);

    const d = desired.length();

    const trueMaxSpeed =
      d < distThreshold
        ? map(d, 0, distThreshold, 0, this.maxSpeed) //@todo apply 1D Nonlinear Transformations
        : this.maxSpeed;

    return desired
      .normalize()
      .multiplyScalar(trueMaxSpeed)
      .sub(this.velocity)
      .clampLength(0, this.maxForce);
  }

  /**
   * @todo Filter with octree here or pass a filtered list ?
   *
   * @param {Array} chasers
   * @param {(undefined|Number)} separation
   * @returns {Vector3}
   */
  separate(chasers, separation = this.neighborhoodRadius) {
    let { sum, closeChasers } = chasers.reduce(
      ({ sum, closeChasers }, chaser) => {
        const d = this.position.distanceTo(chaser.position);
        if (d !== 0 && d <= separation && this !== chaser) {
          const diff = this.position
            .clone()
            .sub(chaser.position)
            .normalize();
          sum.add(diff.divideScalar(d));
          closeChasers++;
        }
        return { sum, closeChasers };
      },
      { sum: new THREE.Vector3(), closeChasers: 0 }
    );

    if (closeChasers > 0) {
      return sum
        .divideScalar(closeChasers)
        .normalize()
        .multiplyScalar(this.maxSpeed)
        .sub(this.velocity)
        .clampLength(0, this.maxForce);
    }
    return new THREE.Vector3();
  }

  /**
   * @param {array} chasers
   * @returns {array}
   */
  getCloseChaser(chasers) {
    return chasers.filter(
      (chaser) =>
        this.position.distanceTo(chaser.position) <= this.neighborhoodRadius
    );
  }
}

class FlowField {
  /**
   * Compute col and row count.
   * May increase width and/or height by less than cell if width/height and cell are not multiple
   *
   * @param {Object} o
   * @param {int} o.width Grid width
   * @param {int} o.height Grid height
   * @param {int} o.cellSize
   * @param {Object} [o.filler=null] A function to fill the flowfield with
   * @param {Array} [o.fields=null] Do not use filler, set fields directly
   */
  constructor({ width, height, cellSize, fields = [], filler = null }) {
    let remainder;

    this.cellSize = cellSize;

    const w = Math.ceil(width),
      h = Math.ceil(height);

    this.width =
      (remainder = w % cellSize) === 0 ? w : w + (cellSize - remainder);
    this.height =
      (remainder = h % cellSize) === 0 ? h : h + (cellSize - remainder);
    this.cols = cellSize > 0 ? this.width / cellSize : 0;
    this.rows = cellSize > 0 ? this.height / cellSize : 0;
    this.fields = fields;

    filler && this.fields.length === 0 && this.fill(filler);
  }

  /**
   * Fill the flow field with callback
   *
   * @param {fill~filler} filler
   */
  /**
   * @callback fill~filler
   * @param {Object} o
   * @param {int} o.curRow
   * @param {int} o.curCol
   * @param {int} o.rows Rows number
   * @param {int} o.cols Cols number
   * @param {int} o.width realWidth
   * @param {int} o.height realHeight
   * @param {int} o.cellSize
   * @param {bool} o.useVector3
   * @returns {(number|Vector2)} The angle in radian or a vector
   */
  fill(filler, useVector3 = false) {
    if (typeof filler !== "function") {
      console.warn("Fill argument must be a function");
      return;
    }

    let { rows, cols, cellSize, width, height } = this;

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        this.fields[i * cols + j] = filler({
          curRow: i,
          curCol: j,
          rows,
          cols,
          width,
          height,
          cellSize,
          useVector3,
        });
      }
    }
  }

  /**
   * @param {*} o
   * @param {Number} o.x
   * @param {Number} o.y
   * @returns {null|Number}
   */
  lookup({ x, y }) {
    if (this.cellSize <= 0) return null;
    if (x < 0 || x >= this.width) return null;
    if (y < 0 || y >= this.height) return null;

    const row = Math.floor(y / this.cellSize);
    const column = Math.floor(x / this.cellSize);
    const index = row * this.cols + column;

    if (!this.fields.hasOwnProperty(index)) return null;

    return this.fields[index];
  }

  /**
   * @todo handle vectors in fields
   */
  toJSON() {
    return {
      cellSize: this.cellSize,
      width: this.width,
      height: this.height,
      cols: this.cols,
      rows: this.rows,
      fields: this.fields,
    };
  }
}

class FlowFieldV3 extends FlowField {
  fill(filler) {
    super.fill(filler, true);
  }

  /**
   * @param {Vector3} position
   * @returns {Vector3}
   */
  lookup(position) {
    if (this.cellSize <= 0) return new THREE.Vector3();
    if (position.x < 0 || position.x >= this.width) return new THREE.Vector3();
    if (position.y < 0 || position.y >= this.height) return new THREE.Vector3();

    const row = Math.floor(position.y / this.cellSize);
    const column = Math.floor(position.x / this.cellSize);
    const index = row * this.cols + column;

    if (!this.fields.hasOwnProperty(index)) return new THREE.Vector3();

    return this.fields[index].clone();
  }

  /**
   * @todo handle vectors in fields
   */
  toJSON() {
    return {
      useVector3: true,
      cellSize: this.cellSize,
      width: this.width,
      height: this.height,
      cols: this.cols,
      rows: this.rows,
      fields: this.fields.map(({ x, y, z }) => ({ x, y, z, isVector3: true })),
    };
  }
}

class Follower extends Chaser {
  /**
   * @param {(FlowFieldV3|Vector3)} flow
   */
  follow(flow) {
    let desired;
    if (flow instanceof FlowFieldV3) {
      desired = flow.lookup(this.position);
    } else if (flow.isVector3) {
      desired = flow;
    } else {
      desired = new THREE.Vector3();
    }
    const steer = desired.multiplyScalar(this.maxSpeed).sub(this.velocity);
    this.applyForce(steer.clampLength(0, this.maxForce));
  }

  // @todo path following
}

class Boid extends Follower {
  constructor({
    separateFactor = 1.5,
    alignFactor = 1,
    cohesionFactor = 1,
    ...params
  }) {
    super(params);
    this.separateFactor = separateFactor;
    this.alignFactor = alignFactor;
    this.cohesionFactor = cohesionFactor;
  }
  /**
   * Flowfield might be used to bound the boid world
   *
   * @param {Array} boids
   * @param {FlowFieldV3} flowField
   */
  flock(boids = [], flowField = null) {
    boids = this.getCloseChaser(boids);

    const s = this.separate(boids);
    const a = this.align(boids);
    const c = this.cohesion(boids);

    s.multiplyScalar(this.separateFactor);
    a.multiplyScalar(this.alignFactor);
    c.multiplyScalar(this.cohesionFactor);

    [s, a, c].forEach((force) => this.applyForce(force));

    if (flowField instanceof FlowFieldV3) {
      this.follow(flowField);
    }
  }

  /**
   * @param {array} boids
   * @returns {Vector3}
   */
  align(boids) {
    let sum = new THREE.Vector3();
    if (boids.length === 0) return sum;
    boids.forEach((boid) => sum.add(boid.velocity));
    return sum
      .divideScalar(boids.length)
      .setLength(this.maxspeed)
      .sub(this.velocity)
      .clampLength(0, this.maxForce);
  }

  /**
   * @param {array} boids
   * @returns {Vector3}
   */
  cohesion(boids) {
    let sum = new THREE.Vector3();
    if (boids.length === 0) return sum;
    boids.forEach((boid) => sum.add(boid.position));
    sum.divideScalar(boids.length);
    return this.chase(sum);
  }
}

class Stack {
  constructor() {
    this.elements = [];
  }
  /**
   * @param {*} state
   */
  push(state) {
    return this.elements.push(state);
  }
  /**
   * @returns {*}
   */
  pop() {
    if (this.elements.length === 0) {
      return null;
    }
    return this.elements.pop();
  }
}

class TurtleState {
  constructor({ segmentStart, segmentEnd, lookAt, width, radius }) {
    this.segmentStart = segmentStart;
    this.segmentEnd = segmentEnd;
    this.lookAt = lookAt;
    this.width = width;
    this.radius = radius;
  }

  clone() {
    return new TurtleState({
      segmentStart: this.segmentStart.clone(),
      segmentEnd: this.segmentEnd.clone(),
      lookAt: this.lookAt.clone(),
      width: this.width,
      radius: this.radius,
    })
  }
}

class LSystem {
  constructor() {
    this.THE_STRING = "";
    this.stack = new Stack();
    this.currentState = null;
    this.startPlace = null;
    this.maxIteration = 0;
    this.currentIteration = 0;
    this.delta = 0;
    this.widthDecrease = 0;
    this.contractionRatio = 0;
    this.initialWidth = 0;
    this.initialRadius = 0;

    this.productions = {};
    this.productionKeys = [];

    this.zAxis = new THREE.Vector3(0, 0, 1);
    this.xAxis = new THREE.Vector3(1, 0, 0);
    this.yAxis = new THREE.Vector3(0, 1, 0);

    this.branchesGeometry = new THREE.Geometry();
    this.leafsGeometry = new THREE.Geometry();
  }

  /**
   * @param {THREE.Vector3} place
   * @returns {LSystem}
   */
  at(place) {
    this.startPlace = place;
    return this;
  }

  create({
    n,
    delta,
    widthDecrease,
    contractionRatio,
    axiom,
    productions,
    initialWidth,
    initialRadius,
  }) {
    this.maxIteration = n;
    this.delta = delta;
    this.widthDecrease = widthDecrease;
    this.contractionRatio = contractionRatio;
    this.productions = productions;
    this.productionKeys = Object.keys(this.productions);
    this.initialWidth = initialWidth;
    this.initialRadius = initialRadius;

    this.buildTheString(axiom);

    return this.render();
  }

  buildTheString(currentString) {
    this.currentIteration++;
    const newString = this.replaceWithProductions(currentString);
    if (this.currentIteration === this.maxIteration) {
      this.THE_STRING = newString;
    } else {
      this.buildTheString(newString);
    }
  }

  replaceWithProductions(currentString) {
    const test = currentString.split("").map((currentChar) => {
      return this.productionKeys.includes(currentChar)
        ? this.productions[currentChar]
        : currentChar;
    });
    return test.join("");
  }

  /**
   * @returns {Object}
   */
  render() {
    this.initState();
    this.THE_STRING.split("").forEach((currentChar) => {
      switch (currentChar) {
        case "[":
          this.pushState();
          break;
        case "]":
          this.popState();
          break;
        case "&":
          this.rotateTurtle("z");
          break;
        case "^":
          this.rotateTurtle("z", "-");
          break;
        case "/":
          this.rotateTurtle("x");
          break;
        case "\\":
          this.rotateTurtle("x", "-");
          break;
        case "+":
          this.rotateTurtle("y");
          break;
        case "-":
          this.rotateTurtle("y", "-");
          break;
        case "!":
          this.decrementSize();
          break;
        case "F":
          this.internode();
          break;
        case "L":
          this.addLeaf();
          break;
        case "A":
          //do nothing
          break;
        case "S":
          //do nothing
          break;

        default:
          console.log(`Unknown letter ${currentChar}`);
          break;
      }
    });

    return {
      leafs: this.leafsGeometry,
      branches: this.branchesGeometry,
    };
  }

  initState() {
    this.currentState = new TurtleState({
      segmentStart: this.startPlace.clone(),
      segmentEnd: new THREE.Vector3(0, 0, 0),
      lookAt: new THREE.Vector3(0, 1, 0),
      width: this.initialWidth,
      radius: this.initialRadius,
    });
  }

  pushState() {
    this.stack.push(this.currentState.clone());
  }

  popState() {
    this.currentState = this.stack.pop();
  }

  rotateTurtle(axis, dir = "+") {
    if (axis === "z") {
      this.currentState.lookAt.applyAxisAngle(
        this.zAxis,
        dir === "+" ? this.delta : -this.delta
      );
    } else if (axis === "y") {
      this.currentState.lookAt.applyAxisAngle(
        this.yAxis,
        dir === "+" ? this.delta : -this.delta
      );
    } else {
      this.currentState.lookAt.applyAxisAngle(
        this.xAxis,
        dir === "+" ? this.delta : -this.delta
      );
    }
  }

  decrementSize() {
    this.currentState.width *= this.widthDecrease;
    this.currentState.radius *= this.contractionRatio;
  }

  addLeaf() {
    const leafGeom = new THREE.PlaneGeometry(0.2, 0.1);
    //Align leaf on the ground
    leafGeom.translate(0, 0.05, 0);

    const leafMesh = new THREE.Mesh(leafGeom);
    //parallel to the ground
    leafMesh.rotateX(Math.PI / 2);

    //put leaf at segment end
    leafMesh.position.copy(this.currentState.segmentEnd);

    //orient with lookat
    // leafMesh.rotateY(angle)

    this.mergeWithOtherLeafs(leafMesh);
  }

  getSegmentCenter(segmentStart, segmentEnd) {
    return new THREE.Vector3(
      (segmentStart.x + segmentEnd.x) / 2,
      (segmentStart.y + segmentEnd.y) / 2,
      (segmentStart.z + segmentEnd.z) / 2
    );
  }

  internode() {
    //move position
    //compute segment end
    const lookAtClone = this.currentState.lookAt.clone();
    lookAtClone.normalize().multiplyScalar(this.currentState.width);

    const start = this.currentState.segmentStart.clone();
    this.currentState.segmentEnd = start.add(lookAtClone);

    //draw branch from start to end
    this.addBranch({ ...this.currentState });

    //move the turtle
    this.currentState.segmentStart = this.currentState.segmentEnd.clone();
  }

  addBranch({ radius, width, segmentStart, lookAt }) {
    const branchGeometry = new THREE.CylinderGeometry(radius, radius, width);
    //Align on the ground
    branchGeometry.translate(0, width / 2, 0);

    const branchMesh = new THREE.Mesh(branchGeometry);
    //then go to actual position
    branchMesh.position.copy(segmentStart);
    //orient the branch
    branchMesh.quaternion.setFromUnitVectors(this.yAxis, lookAt.normalize());

    this.mergeWithTrunk(branchMesh);
  }

  /**
   * @param {THREE.Mesh} leafMesh
   */
  mergeWithOtherLeafs(leafMesh) {
    leafMesh.updateMatrix();
    this.leafsGeometry.merge(leafMesh.geometry, leafMesh.matrix);
  }

  /**
   * @param {THREE.Mesh} branchMesh
   */
  mergeWithTrunk(branchMesh) {
    branchMesh.updateMatrix();
    this.branchesGeometry.merge(branchMesh.geometry, branchMesh.matrix);
  }
}

/**
 * @param {Number} angle
 * @param {bool} useVector3
 */
function toReturn(angle, useVector3) {
  if (angle === null) {
    return useVector3 ? new THREE.Vector3() : null;
  }
  return useVector3 ? new THREE.Vector3(Math.cos(angle), Math.sin(angle)) : angle;
}

/**
 * @param {*} O
 * @param {int} O.curRow
 * @param {int} O.rows
 * @returns {number}
 */
function halfCircleFiller({ curRow, rows, useVector3 }) {
  if (rows === 0) return useVector3 ? new THREE.Vector3() : 0;
  const angle = -(curRow / rows) * Math.PI;
  return toReturn(angle, useVector3);
}

/**
 * Factory function, build the flowfield filler
 *
 * The returned function will fill the flowfield with some curve local slope
 * Compute the formula derivative. Must use "x" as variable name in the passed function.
 *
 * Warning, mathjs is not packaged with that bundle, include it if needed
 *
 * [See mathjs docs]{@link https://mathjs.org/docs/expressions/algebra.html}
 *
 * @param {Object} O
 * @param {string} O.formula - Some math expression with "x" as var. Must be differentiable
 * @param {int} O.width - Flowfield width
 * @param {int} O.height - Flowfield height
 * @param {int} O.cell - Flowfield cell size
 * @param {bool} O.fulfill - Fill cells that are not on formula's curve
 * @param {number} O.maxTilt - Express max tilt applied to the most distant row
 * @returns {Function}
 */
function curveDerivativeFiller({
  formula,
  width,
  height,
  cell,
  fulfill = true,
  maxTilt = 0.5 * Math.PI,
}) {
  if (!width || width <= 0 || !cell || cell <= 0) return null;

  const curve = mathjs.compile(formula);
  const curveDerivative = mathjs.derivative(formula, "x");
  const plot = {};
  const tiltFactor = maxTilt / width / cell;

  //plot point for that curve
  //store nearest row for that computed y
  for (let i = 0; i < width; i += cell) {
    const result = curve.evaluate({ x: i });
    plot[i] =
      result >= 0 && result <= height && cell !== 0
        ? Math.floor(result / cell)
        : null;
  }

  return function fill({ curRow, curCol, rows, cellSize, useVector3 }) {
    if (rows === 0) return 0;

    const x = curCol * cellSize;
    const slope = curveDerivative.evaluate({ x });
    let angle = Math.atan(slope) || 0;

    if (plot[x] === curRow) {
      return toReturn(angle, useVector3);
    } else if (!fulfill) {
      return toReturn(null, useVector3);
    }
    // get distance from current row
    const dist = curRow - plot[x];
    //tilt angle
    angle = tiltFactor * dist + angle;
    return toReturn(angle, useVector3);
  };
}

function edgeRepulsionFiller({ curRow, curCol, rows, cols, cellSize }) {
  if (curRow === 0 && curCol === 0) return; //go bottom right
  if (curRow === 0 && curCol === cols - 1) return; //go bottom left
  if (curRow === rows - 1 && curCol === 0) return; //go up right
  if (curRow === rows - 1 && curCol === cols - 1) return; //go up left
  if (curRow === 0) return; ///go down
  if (curRow === rows - 1) return; //go up
  if (curCol === 0) return; //go right
  if (curCol === cols - 1) return; //go left
}

exports.Boid = Boid;
exports.Chaser = Chaser;
exports.FlowField = FlowField;
exports.FlowFieldV3 = FlowFieldV3;
exports.Follower = Follower;
exports.LSystem = LSystem;
exports.curveDerivativeFiller = curveDerivativeFiller;
exports.edgeRepulsionFiller = edgeRepulsionFiller;
exports.halfCircleFiller = halfCircleFiller;
