import React, { useCallback } from "react";
import { useForm } from "react-hook-form";

export default function FlowFieldForm({ useStore, defaults }) {
  const updateFields = useStore((state) => state.updateFields);
  const onSubmit = useCallback(
    (data) => {
      const d = {
        ...data,
        cell: parseInt(data.cell),
        width: parseInt(data.width),
        height: parseInt(data.height),
      };
      updateFields(d);
    },
    [updateFields]
  );
  const { register, handleSubmit } = useForm();

  return (
    <form onSubmit={handleSubmit(onSubmit)} style={{ marginBottom: "2rem" }}>
      <Input
        label="Width"
        name="width"
        register={register}
        defaultValue={defaults.width}
      />
      <Input
        label="Height"
        name="height"
        register={register}
        defaultValue={defaults.height}
      />
      <Input
        label="Cell Size"
        name="cell"
        register={register}
        defaultValue={defaults.cell}
      />
      <Input
        label="The math voodoo"
        name="formula"
        register={register}
        defaultValue={defaults.formula}
      />
      <Input
        label="Max tilt applyed if fullfill is true"
        name="maxTilt"
        register={register}
        defaultValue={defaults.maxTilt}
      />
      <Checkbox
        label={"Fulfill flow field outside of the curve"}
        name={"fulfill"}
        register={register}
        defaultChecked={defaults.fulfill}
      />
      <Checkbox
        label={"Use vector (default rad)"}
        name={"useVector3"}
        register={register}
        defaultChecked={false}
      />
      <input type="submit" />
    </form>
  );
}

const Input = ({ label, name, register, required, ...props }) => (
  <p>
    <label htmlFor={name} style={{ display: "inline-block", width: "33%" }}>
      {label}
    </label>
    <input name={name} ref={register({ required })} {...props} />
  </p>
);

const Checkbox = ({ label, name, register, required, ...props }) => (
  <p>
    <label htmlFor={name} style={{ display: "inline-block", width: "33%" }}>
      {label}
    </label>
    <input
      type="checkbox"
      name={name}
      ref={register({ required })}
      {...props}
    />
  </p>
);
