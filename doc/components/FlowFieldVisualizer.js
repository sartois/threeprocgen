import React, { useMemo } from "react";
import { Canvas, extend, useThree } from "react-three-fiber";
import { evaluate } from "mathjs";
import create from "zustand";
import { FlowField, curveDerivativeFiller, FlowFieldV3 } from "../../src/index";
import { FlowFieldHelper } from "../../src/FlowField/helper/FlowFieldHelper";
import FlowFieldForm from "./FlowFieldForm";
import FlowFieldOutput from "./FlowFieldOutput";

extend({ FlowFieldHelper });

const defaultVizState = {
  formula: "100*sin(0.005x)+300",
  width: 600,
  height: 400,
  cell: 20,
  fulfill: true,
  maxTilt: "0.5 * pi",
};

const colorPalette = {
  lightBlue: "#B8DEFF",
  deepBlue: "#55AFFF",
  paleOrange: "#FFCBB3",
  deepOrange: "#FF8148",
  paleYellow: "#FFEDB3",
  deepYellow: "#FFD348",
};

const useStore = create((set) => ({
  fields: defaultVizState,
  updateFields: (fields) => set(() => ({ fields: { ...fields } })),
  flowField: "",
  updateFlowField: (flowField) =>
    set(() => ({
      flowField,
    })),
}));

//@todo go responsive, all size are hardcoded for a typical desktop screen
export default function FlowFieldVisualizer({ debug = false }) {
  return (
    <section>
      <FlowFieldForm useStore={useStore} defaults={defaultVizState} />
      <Canvas
        gl={{ alpha: false, antialias: false }}
        camera={{
          fov: 60,
          position: [0, 500, 0],
          near: 10,
          far: 10000,
        }}
        style={{ height: "810px" }}
        onCreated={({ gl }) => gl.setClearColor(colorPalette.lightBlue)}
      >
        <FlowFieldViz />
        <PositionCamera />
        {debug && <axesHelper args={[200]} />}
      </Canvas>
      <FlowFieldOutput useStore={useStore} />
    </section>
  );
}

//Put origin in the top right corner
//Y goes down. Is that bad ?
function PositionCamera() {
  const { camera } = useThree();
  useMemo(() => {
    camera.translateX(400);
    camera.translateY(-250);
  }, [camera]);

  return null;
}

function FlowFieldViz() {
  const updateFlowField = useStore((state) => state.updateFlowField);
  const {
    width,
    height,
    cell,
    formula,
    fulfill,
    maxTilt,
    useVector3,
  } = useStore((state) => state.fields);
  const filler = useMemo(() => {
    let mMaxTilt = "";
    try {
      mMaxTilt = evaluate(maxTilt);
    } catch (e) {
      console.error(e);
      mMaxTilt = maxTilt;
    }
    return curveDerivativeFiller({
      formula,
      width,
      height,
      cell,
      fulfill,
      maxTilt: mMaxTilt,
    });
  }, [width, height, cell, formula, fulfill, maxTilt]);

  const flowField = useMemo(() => {
    const args = {
      width,
      height,
      cellSize: cell,
      filler,
    };
    const flowfield = useVector3 ? new FlowFieldV3(args) : new FlowField(args);
    updateFlowField(JSON.stringify(flowfield));
    return flowfield;
  }, [width, height, cell, filler, updateFlowField, useVector3]);

  return (
    <flowFieldHelper
      args={[flowField, colorPalette.deepBlue, colorPalette.deepOrange, true]}
    />
  );
}
