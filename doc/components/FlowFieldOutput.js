import React from "react";

export default function FlowFieldOutput({ useStore }) {
  const flowField = useStore((state) => state.flowField);

  return (
    <form style={{ width: "100%" }}>
      <p>
        <label htmlFor="ffOutput">
          Serialized flowfield (use factory to instanciate)
        </label>
      </p>
      <textarea
        name="ffOutput"
        style={{ width: "100%" }}
        value={flowField}
        readOnly
      ></textarea>
    </form>
  );
}
