import { Follower, FlowFieldV3, Chaser } from "../src/index";
import { Vector3 } from "three";

describe("Follower", () => {
  test("import and creation should not throw error", () => {
    expect(new Follower({})).toBeInstanceOf(Chaser);
    expect(new Follower({})).toBeInstanceOf(Follower);
  });

  test("follow should add to acceleration", () => {
    const force = new Vector3(0, 5, 0);
    const ff = new FlowFieldV3({
      width: 10,
      height: 10,
      cellSize: 10,
      filler: () => force,
    });
    const agent = new Follower({ initialPosition: new Vector3(5, 5, 0) });

    expect(agent.acceleration.equals(new Vector3())).toBe(true);
    agent.follow(ff);
    expect(agent.acceleration.equals(force)).toBe(true);
  });
});
