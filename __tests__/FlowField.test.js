import { FlowField, halfCircleFiller, flowfieldFactory } from "../src/index";

describe("circleFiller", () => {
  test("should return predictable angle in radian", () => {
    expect(halfCircleFiller({ curRow: 1, rows: 1 })).toBe(-Math.PI);
    expect(halfCircleFiller({ curRow: 1, rows: 2 })).toBe(-Math.PI / 2);
    expect(halfCircleFiller({ curRow: 1, rows: 0 })).toBe(0);
  });
});

describe("FlowField", () => {
  test("import and creation should not throw error", () => {
    expect(new FlowField({})).toBeInstanceOf(FlowField);
  });

  test("grid size should be OK with cell size multiple of width or height", () => {
    const size = 100;
    const c = 5;
    const flowField = new FlowField({
      width: size,
      height: size,
      cellSize: c,
    });

    expect(flowField.width).toBe(size);
    expect(flowField.height).toBe(size);
    expect(flowField.cols).toBe(size / c);
    expect(flowField.rows).toBe(size / c);
  });

  test("grid size should be OK with cell size not multiple of width or height", () => {
    const size = 101;
    const c = 5;
    const flowField = new FlowField({
      width: size,
      height: size,
      cellSize: c,
    });

    expect(flowField.width).toBe(size + (c - (size % c)));
    expect(flowField.height).toBe(size + (c - (size % c)));
    expect(flowField.cols).toBe(flowField.width / c);
    expect(flowField.rows).toBe(flowField.width / c);
  });

  test("grid size should be empty if filler is not set", () => {
    const size = 20;
    const c = 5;
    const flowField = new FlowField({
      width: size,
      height: size,
      cellSize: c,
    });
    expect(flowField.fields).toHaveLength(0);
  });

  test("fields should have cols*rows length", () => {
    const size = 20;
    const c = 5;
    const flowField = new FlowField({
      width: size,
      height: size,
      cellSize: c,
      filler: () => 1,
    });
    expect(flowField.fields).toHaveLength(flowField.cols * flowField.rows);
  });

  test("fields should not throw error even with out of range lookup position", () => {
    const size = 10;
    const c = 5;
    const flowField = new FlowField({
      width: size,
      height: size,
      cellSize: c,
      filler: ({ curRow, curCol, cols }) => curRow * cols + curCol,
    });
    expect(flowField.lookup({ x: -1, y: -1 })).toBe(null);
    expect(flowField.lookup({ x: 2.5, y: -0.1 })).toBe(null);
    expect(flowField.lookup({ x: 0, y: 0 })).toBe(0);
    expect(flowField.lookup({ x: 1, y: 0 })).toBe(0);
    expect(flowField.lookup({ x: 0, y: 1 })).toBe(0);
    expect(flowField.lookup({ x: 1, y: 1 })).toBe(0);

    expect(flowField.lookup({ x: 5.1, y: 0.1 })).toBe(1);
    expect(flowField.lookup({ x: 7.5, y: 2.5 })).toBe(1);
    expect(flowField.lookup({ x: 5.1, y: -1 })).toBe(null);
    expect(flowField.lookup({ x: 10, y: 5 })).toBe(null);
    expect(flowField.lookup({ x: 10.1, y: 0.1 })).toBe(null);

    expect(flowField.lookup({ x: 4, y: 5 })).toBe(2);
    expect(flowField.lookup({ x: 0.1, y: 5.1 })).toBe(2);
    expect(flowField.lookup({ x: 0, y: 5.1 })).toBe(2);
    expect(flowField.lookup({ x: 2.5, y: 7.5 })).toBe(2);
    expect(flowField.lookup({ x: -0.1, y: 5.5 })).toBe(null);
    expect(flowField.lookup({ x: -0.1, y: 10.1 })).toBe(null);
    expect(flowField.lookup({ x: 2.5, y: 10.1 })).toBe(null);
    expect(flowField.lookup({ x: 5, y: 10 })).toBe(null);

    expect(flowField.lookup({ x: 5, y: 5 })).toBe(3);
    expect(flowField.lookup({ x: 6, y: 5 })).toBe(3);
    expect(flowField.lookup({ x: 5.1, y: 5.1 })).toBe(3);
    expect(flowField.lookup({ x: 7.5, y: 10.1 })).toBe(null);
    expect(flowField.lookup({ x: 10.1, y: 7.5 })).toBe(null);
    expect(flowField.lookup({ x: 10.1, y: 10.1 })).toBe(null);
  });

  /* test("should be correct when built from flowfieldFactory", () => {
    const input =
      '{"cellSize":20,"width":40,"height":40,"fields":[0.4636476090008061,0.46164727911384046,0.4656111044092997,0.46361077452233407],"cols":2,"rows":2}';
    const output = flowfieldFactory(input);

    expect(output).toBeInstanceOf(FlowField);
    expect(output.cellSize).toBe(20);
    expect(output.width).toBe(40);
    expect(output.height).toBe(40);
    expect(Array.isArray(output.fields)).toBe(true);
    expect(output.cols).toBe(2);
    expect(output.rows).toBe(2);
  });*/
});
