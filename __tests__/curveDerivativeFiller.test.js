//the mock always returns an object with an evaluate function that returns 1
jest.mock("mathjs");

import { curveDerivativeFiller } from "../src/index";

describe("curveDerivativeFiller", () => {
  test("should return null if width or cell are less than zero or falsy", () => {
    expect(
      curveDerivativeFiller({
        width: 100,
        cell: 0,
      })
    ).toBe(null);
    expect(
      curveDerivativeFiller({
        width: -1,
        cell: 10,
      })
    ).toBe(null);
    expect(curveDerivativeFiller({})).toBe(null);
  });

  test("should return a function if called with args", () => {
    const result = curveDerivativeFiller({
      width: 100,
      formula: "x",
      height: 100,
      cell: 10,
    });
    expect(result instanceof Function).toBe(true);
  });

  test("should return a filler that work", () => {
    const filler = curveDerivativeFiller({
      width: 10,
      formula: "x",
      height: 10,
      cell: 10,
    });

    expect(filler({ curRow: 0, curCol: 0, rows: 1, cellSize: 10 })).toBe(
      Math.atan(1)
    );
  });
});
