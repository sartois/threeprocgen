import { FlowFieldV3 } from "../src/index";
import { Vector3 } from "three";

describe("FlowFieldV3", () => {
  test("import and creation should not throw error", () => {
    expect(new FlowFieldV3({})).toBeInstanceOf(FlowFieldV3);
  });

  test("lookup should return empty Vector3 if lookup is called from a out of range position", () => {
    let flowField = new FlowFieldV3({
      cellSize: 0,
    });
    let result = flowField.lookup({ x: 1, y: 1 });
    expect(result).toBeInstanceOf(Vector3);
    expect(result.length()).toBe(0);

    flowField = new FlowFieldV3({
      cellSize: 5,
      width: 10,
      height: 10,
    });

    result = flowField.lookup({ x: -1, y: 1 });
    expect(result).toBeInstanceOf(Vector3);
    expect(result.length()).toBe(0);

    result = flowField.lookup({ x: 11, y: 1 });
    expect(result).toBeInstanceOf(Vector3);
    expect(result.length()).toBe(0);

    result = flowField.lookup({ x: 1, y: -1 });
    expect(result).toBeInstanceOf(Vector3);
    expect(result.length()).toBe(0);

    result = flowField.lookup({ x: 1, y: 11 });
    expect(result).toBeInstanceOf(Vector3);
    expect(result.length()).toBe(0);

    //no filler passed, this.fields is empty
    result = flowField.lookup({ x: 1, y: 1 });
    expect(result).toBeInstanceOf(Vector3);
    expect(result.length()).toBe(0);
  });

  test("lookup should return a Vector3 if lookup is called within bounds", () => {
    const mockClone = jest.fn(() => null);
    const flowField = new FlowFieldV3({
      cellSize: 5,
      width: 10,
      height: 10,
      filler: () => ({ clone: mockClone }),
    });

    const result = flowField.lookup({ x: 1, y: 1 });

    expect(mockClone.mock.calls.length).toBe(1);
    expect(result).toBe(null);
  });
});
