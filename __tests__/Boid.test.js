import { Boid } from "../src/index";

describe("Boid", () => {
  test("Boid import and creation should not throw error", () => {
    expect(new Boid({})).toBeInstanceOf(Boid);
  });
});
