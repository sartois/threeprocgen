import { Chaser } from "../src/index";
import { Vector3 } from "three";

let chaser, target;

describe("Chaser", () => {
  beforeEach(() => {
    chaser = new Chaser({ maxSpeed: 5, maxForce: 5 });
    target = new Vector3(10, 10, 10);
  });

  test("import and creation should not throw error", () => {
    expect(new Chaser({})).toBeInstanceOf(Chaser);
  });

  test("reach should return Vector3", () => {
    expect(chaser.reach(target)).toBeInstanceOf(Vector3);
  });

  test("reach should not change target", () => {
    const targetCopy = target.clone();
    const result = chaser.reach(target);
    expect(result === target).toBe(false);
    expect(target.equals(targetCopy)).toBe(true);
  });

  test("reach should return something coherent", () => {
    const result = chaser.reach(new Vector3(0, 100, 0));
    //target is further than arriveThreshold (100), velocity is 0,0,0, so agent must go Y by maxSpeed
    expect(result.equals(new Vector3(0, chaser.maxSpeed, 0))).toBe(true);
  });

  test("reach should return something coherent if target is less far than arriveThreshold", () => {
    const result = chaser.reach(new Vector3(0, 50, 0));
    expect(result.equals(new Vector3(0, chaser.maxSpeed / 2, 0))).toBe(true);
  });

  test("reach should use passed threshold if any and not internal arriveThreshold", () => {
    let result = chaser.reach(new Vector3(0, 50, 0), 0);
    //threshold is 0, only use maxSpeed
    expect(result.equals(new Vector3(0, chaser.maxSpeed, 0))).toBe(true);

    result = chaser.reach(new Vector3(0, 50, 0), 200);
    //threshold is 4x bigger
    expect(result.equals(new Vector3(0, chaser.maxSpeed / 4, 0))).toBe(true);
  });

  test("update should be predictable", () => {
    chaser.applyForce(new Vector3(10, 0, 0));
    chaser.update();
    expect(chaser.acceleration.equals(new Vector3())).toBe(true);
    expect(chaser.velocity.equals(new Vector3(5, 0, 0))).toBe(true);
    expect(chaser.position.equals(new Vector3(5, 0, 0))).toBe(true);
  });

  test("getCloseChaser should filter as expected", () => {
    const chasers = [
      new Chaser({ initialPosition: new Vector3(5, 0, 0) }),
      new Chaser({ initialPosition: new Vector3(101, 0, 0) }),
    ];

    const result = chaser.getCloseChaser(chasers);
    expect(result).toHaveLength(1);
    expect(result[0]).toBe(chasers[0]);
  });

  test("separate should be predictable", () => {
    const chasers = [
      new Chaser({ initialPosition: new Vector3(5, 0, 0) }),
      new Chaser({ initialPosition: new Vector3(101, 0, 0) }),
    ];

    const result = chaser.separate(chasers);

    expect(result).toBeInstanceOf(Vector3);
    //Only one chaser is close enough, so we must go oposite dir by maxSpeed
    expect(result.equals(new Vector3(-5, 0, 0))).toBe(true);
  });
});
